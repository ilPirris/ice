package com.plusimple.ice.managers;

import com.plusimple.ice.managers.specs.AbstractManager;
import com.plusimple.ice.models.IceCode;
import com.plusimple.ice.models.User;
import com.plusimple.ice.repositories.interfaces.specs.AbstractDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserManager extends AbstractManager<User>
{

    public User findByCode(IceCode iceCode)
    {
        return dao.findOne(iceCode.getUser().getId());

    }

}
