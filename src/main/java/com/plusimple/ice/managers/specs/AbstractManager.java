package com.plusimple.ice.managers.specs;


import com.plusimple.ice.models.specs.AbstractEntity;
import com.plusimple.ice.repositories.interfaces.specs.AbstractDao;
import com.plusimple.ice.repositories.specs.AbstractRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.MappedSuperclass;

@MappedSuperclass
@Transactional
public abstract class AbstractManager<Model extends AbstractEntity> {

    @Autowired
    protected AbstractRepository<Model> repository;

    @Autowired
    protected AbstractDao<Model> dao;
}
