package com.plusimple.ice.managers;

import com.plusimple.ice.managers.specs.AbstractManager;
import com.plusimple.ice.models.IceCode;
import com.plusimple.ice.repositories.IceCodeRepository;
import com.plusimple.ice.repositories.UserRepository;
import com.plusimple.ice.repositories.interfaces.IceCodeDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class IceCodeManager extends AbstractManager<IceCode>{


    public  boolean exists(String code)
    {
        if(((IceCodeDao)dao).findByCode(code) == null)
            return false;
        else
            return true;
    }

    public IceCode findByCode(String code)
    {
        return ((IceCodeDao)dao).findByCode(code);

    }
}
