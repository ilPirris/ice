package com.plusimple.ice.repositories;

import com.plusimple.ice.models.User;
import com.plusimple.ice.repositories.specs.AbstractRepository;
import org.springframework.stereotype.Repository;



@Repository
public class UserRepository extends AbstractRepository<User>
{


}
