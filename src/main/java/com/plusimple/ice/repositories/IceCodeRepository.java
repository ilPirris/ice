package com.plusimple.ice.repositories;

import com.plusimple.ice.models.IceCode;
import com.plusimple.ice.repositories.interfaces.IceCodeDao;
import com.plusimple.ice.repositories.specs.AbstractRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;



@Repository
public class IceCodeRepository extends AbstractRepository<IceCode>{

}
