package com.plusimple.ice.repositories.specs;

import com.plusimple.ice.models.specs.AbstractEntity;

import org.apache.log4j.Logger;
import org.springframework.data.repository.NoRepositoryBean;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.Collections;
import java.util.List;


@NoRepositoryBean
public abstract class AbstractRepository<Model extends AbstractEntity> {

    @PersistenceContext
    protected EntityManager em;

    //protected Logger logger = Logger.getLogger(this.getClass().getSimpleName());

    protected Class<Model> ModelClass;

    public List<Model> findAllByQuery(Query q)
    {
        List<Model> results = null;

        try
        {
            results = q.getResultList();
        }
        catch (Exception e)
        {
            //logger.error(ModelClass.getSimpleName() + "Repository: " + e.getMessage());
            e.printStackTrace();


            return Collections.emptyList();
        }

        return results;
    }
}
