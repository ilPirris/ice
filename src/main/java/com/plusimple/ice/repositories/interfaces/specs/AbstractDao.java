package com.plusimple.ice.repositories.interfaces.specs;

import com.plusimple.ice.models.specs.AbstractEntity;
import org.springframework.data.jpa.repository.JpaRepository;




public interface AbstractDao<Model extends AbstractEntity> extends JpaRepository<Model, Long>
{
}
