package com.plusimple.ice.repositories.interfaces;

import com.plusimple.ice.models.User;
import com.plusimple.ice.repositories.interfaces.specs.AbstractDao;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.transaction.Transactional;



@Transactional
public interface UserDao extends AbstractDao<User>
{

}
