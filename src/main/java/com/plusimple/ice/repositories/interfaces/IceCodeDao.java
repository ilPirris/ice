package com.plusimple.ice.repositories.interfaces;

import com.plusimple.ice.models.IceCode;
import com.plusimple.ice.repositories.interfaces.specs.AbstractDao;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.transaction.Transactional;



@Transactional
public interface IceCodeDao extends AbstractDao<IceCode> {
    public IceCode findByCode(String code);
}
