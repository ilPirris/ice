package com.plusimple.ice.models;

import com.plusimple.ice.models.specs.AbstractEntity;
import org.hibernate.search.annotations.Field;


import javax.persistence.*;


@Entity
@Table(name = "ICE_CODE")
public class IceCode extends AbstractEntity
{


        @OneToOne(fetch = FetchType.EAGER)
        User user;

        @Field
        String code;



        public User getUser() {
            return user;
        }

        public void setUser(User user) {
            this.user = user;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

}
