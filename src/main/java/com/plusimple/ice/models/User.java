package com.plusimple.ice.models;

import com.plusimple.ice.models.specs.AbstractEntity;
import org.hibernate.search.annotations.Field;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * Created by Plusimple on 28/07/2016.
 */

@Entity
@Table(name = "USER")
public class User extends AbstractEntity {


    @Field
    String email;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
