package com.plusimple.ice.api.v1.controllers.specs;

import com.plusimple.ice.managers.specs.AbstractManager;
import com.plusimple.ice.models.specs.AbstractEntity;
import org.springframework.beans.factory.annotation.Autowired;


public abstract class AbstractController<Model extends AbstractEntity> {

    @Autowired
    protected AbstractManager<Model> manager;
}
