package com.plusimple.ice.api.v1.controllers;

import com.plusimple.ice.api.v1.controllers.specs.AbstractController;
import com.plusimple.ice.api.v1.exceptions.HttpForbiddenException;
import com.plusimple.ice.managers.IceCodeManager;
import com.plusimple.ice.managers.UserManager;
import com.plusimple.ice.models.IceCode;
import com.plusimple.ice.models.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.jws.soap.SOAPBinding;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

@RestController
@RequestMapping("/user")
public class UserController extends AbstractController<User> {

    @Autowired
    IceCodeManager iceCodeManager;


    @RequestMapping("/info")
    public User getPatient(@CookieValue(value = "code", defaultValue = "noValue") String code)
    {
        if (!code.contains("noValue")) {
            IceCode iceCode = iceCodeManager.findByCode(code);
            return iceCode.getUser();
        }
        else
            throw new HttpForbiddenException();
    }

    @RequestMapping("/{code}")
    public String login(@PathVariable String code, HttpServletResponse response)
    {
        if (iceCodeManager.exists(code)) {
            Cookie cookie = new Cookie("code", code);
            response.addCookie(cookie);

            return code;
        }
        else
            throw new HttpForbiddenException();
    }

}
